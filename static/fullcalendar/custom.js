$(function() { // document ready

  ZOOM_FACTOR = 2.5,
  MAX_CELLS = 1000,
  INTERVAL_WIDTH = 35,
  INTERVAL_DURATIONS = [
    moment.duration(15, 'minutes'),
    moment.duration(30, 'minutes'),
    moment.duration(1, 'hours'),
    moment.duration(3, 'hours'),
  ];

  $('#calendar').fullCalendar({
    schedulerLicenseKey: 'CC-Attribution-NonCommercial-NoDerivatives',
    resourceAreaWidth: 200,
    contentHeight: 750,
    minTime: '08:00:00',
    maxTime: '20:00:00',
    slotDuration: '00:15:00',
    firstDay: 1,
    nowIndicator: true,
    editable: false, // enable draggable events
    scrollTime: '00:00', // undo default 6am scrollTime

    header: {
      left: 'today prev,next, zoomOut,zoomIn',
      center: 'title',
      right: 'timelineDay,timelineThreeDays,timelineSevenDays, agendaWeek,month, listWeek'
    },
    customButtons: {
       zoomOut: {
        text: '-',
        click: function () {
          zoomOut();
        }
      },
      zoomIn: {
        text: '+',
        click: function () {
          zoomIn();
        }
      }
    },
    defaultView: 'timelineDay',
    views: {
      timelineThreeDays: {
        type: 'timeline',
        duration: { days: 3 }
      },
      timelineSevenDays: {
        type: 'timeline',
        duration: { days: 7 }
      }
    },

    resourceLabelText: 'Rooms',
    resources: mpg_resources,
    events: mpg_events,

    eventClick: function(calEvent, jsEvent, view) {
      $('#successModal').modal('show');
      $('#successModal .modal-body p').text("Resource ID: " + calEvent.title);
    }

  });

  function zoomIn (zoomPoint) {
    var zoomValues = getZoomValues(zoomPoint);

    handleZoom(
      {
        start: moment(zoomValues.zoomPoint - (zoomValues.viewportDuration / (2 * ZOOM_FACTOR))),
        end: moment(zoomValues.zoomPoint + (zoomValues.viewportDuration / (2 * ZOOM_FACTOR)))
      }
    );
  }

  function zoomOut (zoomPoint) {
    var zoomValues = getZoomValues(zoomPoint);

    handleZoom(
      {
        start: moment(zoomValues.zoomPoint - (zoomValues.viewportDuration / (2 / ZOOM_FACTOR))),
        end: moment(zoomValues.zoomPoint + (zoomValues.viewportDuration / (2 / ZOOM_FACTOR)))
      }
    );
  }

  function getTimelineInfo () {
    var elem = $('#calendar'),
        viewportElem = elem.find('.fc-time-area .fc-scroller'),
        timelineElem = elem.find('.fc-time-area .fc-scroller-canvas'),
        timelineStart = elem.fullCalendar('getView').start,
        timelineEnd = elem.fullCalendar('getView').end;

    return {
      elem: elem,
      viewport: viewportElem,
      timeline: timelineElem,
      viewportWidth: viewportElem.width(),
      timelineWidth: timelineElem.width(),
      timelineDuration: moment.duration(timelineEnd.diff(timelineStart)),
      timelineStart: timelineStart.clone(),
      timelineEnd: timelineEnd.clone()
    };
  }

  function getZoomValues (zoomPoint) {
    var viewportCenterDate = getViewportCenterDate(),
        viewportDuration = getViewportDuration(),
        viewportStart = viewportCenterDate.clone().subtract(viewportDuration / 2),
        viewportEnd = viewportCenterDate.clone().add(viewportDuration / 2);

    //calculate center of current viewport if not given
    if (zoomPoint === undefined)
      zoomPoint = viewportCenterDate;

    //clip to zoom in current viewport
    if (zoomPoint < viewportStart)
      zoomPoint = viewportStart;
    else if (zoomPoint > viewportEnd)
      zoomPoint = viewportEnd;

    return {
      zoomPoint: zoomPoint,
      viewportDuration: viewportDuration
    };
  }

  function getViewportDuration () {
    var timelineInfo = getTimelineInfo(),
        viewportTimelinePercentage = timelineInfo.viewportWidth / timelineInfo.timelineWidth;

    return moment.duration(timelineInfo.timelineDuration * viewportTimelinePercentage);
  }

  function getViewportCenterDate () {
    var timelineInfo = getTimelineInfo(),
        vpScrollLeft = timelineInfo.viewport.scrollLeft(),
        viewportCenterOffsetPercentage = (vpScrollLeft + timelineInfo.viewportWidth / 2) / timelineInfo.timelineWidth,
        dateOffset = viewportCenterOffsetPercentage * timelineInfo.timelineDuration;

    return timelineInfo.timelineStart.clone().add(dateOffset);
  }

  function handleZoom (viewport) {
    var timelineInfo = getTimelineInfo(),
        viewportDuration = moment.duration(viewport.end - viewport.start);

    if (viewport.start < timelineInfo.timelineStart) {
      viewport.start = timelineInfo.timelineStart.clone();
      viewport.end = viewport.start.clone().add(viewportDuration);
    }

    if (viewport.end > timelineInfo.timelineEnd) {
      viewport.end = timelineInfo.timelineEnd.clone();
      viewport.start = moment(Math.max(viewport.end.clone().subtract(viewportDuration), timelineInfo.timelineStart));
    }

    var calculated = {
      viewportDuration: moment.duration(viewport.end.diff(viewport.start)),
      timelineDuration: moment.duration(timelineInfo.timelineEnd.diff(timelineInfo.timelineStart))
    };
    console.log('Viewport duration :', getViewportDuration().asHours());
    console.log('Viewport width :', timelineInfo.viewportWidth);

    //Calculate the optimal slotwidth given the width constant
    calculated.numIntervalsPreffered = Math.ceil(timelineInfo.viewportWidth / INTERVAL_WIDTH);

    //Calculate the optimal duration for each slot
    calculated.optimalIntervalDuration = moment.duration(calculated.viewportDuration / calculated.numIntervalsPreffered);

    //Find the closest interval compared to the optimal interval
    calculated.intervalDuration = findClosestIntervalDuration(calculated.optimalIntervalDuration);

    //calculate the amount of intervals fitting in the viewport
    calculated.viewportNumIntervals = calculated.viewportDuration / calculated.intervalDuration;

    //Calculate how many pixels each slot should be todo: Math.ceil ? check specs
    calculated.intervalWidth = Math.round(timelineInfo.viewportWidth / calculated.viewportNumIntervals);

    calculated.timelineNumIntervals = calculated.timelineDuration / calculated.intervalDuration;
    calculated.timelineWidth = calculated.timelineNumIntervals * calculated.intervalWidth;

    //Check for overflowing, if there are more slots when allowed don't add more, just make them wider.
    while ($.fullCalendar.divideDurationByDuration(timelineInfo.timelineDuration, calculated.intervalDuration) > MAX_CELLS) {
      for (var i = 0; i < INTERVAL_DURATIONS.length; i++) {
        if (INTERVAL_DURATIONS[i] <= calculated.intervalDuration)
          continue;

        calculated.intervalDuration = INTERVAL_DURATIONS[i];
        break;
      }

      calculated.viewportNumIntervals = calculated.viewportDuration / calculated.intervalDuration;
      calculated.timelineNumIntervals = calculated.timelineDuration / calculated.intervalDuration;
      calculated.intervalWidth = Math.round(timelineInfo.viewportWidth / calculated.viewportNumIntervals);
      calculated.timelineWidth = calculated.timelineNumIntervals * calculated.intervalWidth;
    }

    //Calculate the scroll position given the new viewport (keeps the old view in the middle of the screen)
    calculated.viewportLeftOffset = ((viewport.start - timelineInfo.timelineStart) / calculated.timelineDuration) * calculated.timelineWidth

    for(var key in calculated) {
      console.log(key, ': ', moment.isDuration(calculated[key]) ? calculated[key].asHours() : calculated[key]);
    }
    console.log('===============');

    $('#calendar').fullCalendar('option', {
      slotWidth: calculated.intervalWidth,
      slotLabelInterval: calculated.intervalDuration,
      slotDuration: calculated.intervalDuration
    });

    //In my own code the timeOut was not needed, but it has to wait for the view to render (which is async)
    setTimeout(function () {
        $('#calendar').find('.fc-time-area .fc-scroller').scrollLeft(calculated.viewportLeftOffset);
    });
  }

  function findClosestIntervalDuration (optimalDuration) {
    var closest = INTERVAL_DURATIONS[0];

    for (var i = 1; i < INTERVAL_DURATIONS.length; i++) {
      if (Math.abs(INTERVAL_DURATIONS[i] - optimalDuration) < Math.abs(closest - optimalDuration))
        closest = INTERVAL_DURATIONS[i];
    }

    return moment.duration(closest);
  }

});
