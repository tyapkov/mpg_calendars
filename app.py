import os

from flask import Flask
from flask import render_template
from icalendar import Calendar

import getpass

import config


import urllib

app = Flask(__name__)
cl = config.SCHEDULES_PATH
resources = config.SCHEDULES_RESOURCES

# filters file and returns events from
# start date to end date
def filterIscFile(path, resource_id, room_name, index):

    g = None
    if path.startswith("https"):
        # try to open from url

        auth_user = input('Username: ')
        auth_passwd = getpass.getpass('Password: ')

        # https://docs.python.org/3.4/howto/urllib2.html#id5
        passman = urllib.request.HTTPPasswordMgrWithDefaultRealm()
        passman.add_password(None, path, auth_user, auth_passwd)
        authhandler = urllib.request.HTTPBasicAuthHandler(passman)
        opener = urllib.request.build_opener(authhandler)
        urllib.request.install_opener(opener)

        g = urllib.request.urlopen(path)
    else:
        g = open(path, 'rb')

    gcal = Calendar.from_ical(g.read())
    events = []
    for component in gcal.walk():
        index += 1
        if component.name == "VEVENT":
            event = {}
            event['id'] = str(index)
            event['resourceId'] = resource_id
            event['title'] = str(component.get('organizer') + ' at ' + room_name)
            event['start'] = component.get('dtstart').dt.strftime("%Y-%m-%d %H:%M:%S")
            event['end'] = component.get('dtend').dt.strftime("%Y-%m-%d %H:%M:%S")
            events.append(event)
    return events, index

def get_events(prefix=None):
    result = []
    index = 1
    for resource in resources:
        if prefix:
            # show only for given resource
            if resource['id'] == prefix:
                for room in resource['children']:
                    events, index = filterIscFile(os.fsdecode(room['link']), room['id'], room['title'], index)
                    result.extend(events)
        else:
            # show all
            for room in resource['children']:
                events, index = filterIscFile(os.fsdecode(room['link']), room['id'], room['title'], index)
                result.extend(events)

    return result

# either return all resources
# or resources where id = prefix
def get_resources(prefix):
    if prefix:
        return [d for d in resources if d['id'] in [prefix]]
    return resources


@app.route("/")
def main():
    return render_template(
            'index.html',
            prefix = 'all',
            events = get_events(),
            resources = resources,
    )

@app.route("/<prefix>/")
def show_events(prefix):
    return render_template(
            'index.html',
            prefix = prefix,
            events = get_events(prefix),
            resources = get_resources(prefix),
    )

if __name__ == "__main__":
    app.run(host='0.0.0.0')
