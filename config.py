SCHEDULES_RESOURCES = [

    # {'id': 'lh', 'title': 'Lecture halls', 'children': [
    #     {'id': 'lh1', 'title': 'Lecture hall', 'link': 'https://zimbra.cbs.mpg.de/dav/tyapkov/lecturehall', 'eventColor': 'black'},
    # ]},

    {'id': 'mrt', 'title': 'MRTs', 'children': [
        {'id': 'mrt1', 'title': 'MRT1 / MRT1 (Connectom)', 'link': cl + 'MRT1.ics', 'eventColor': 'orange'},
        {'id': 'mrt2', 'title': 'B012 / MRT2 (Prisma)', 'link':cl + 'B012.ics', 'eventColor': 'orange'},
        {'id': 'mrt3', 'title': 'B032 / MRT3 (7T)', 'link': cl + 'B032.ics', 'eventColor': 'orange'},
        {'id': 'mrt4', 'title': 'b047 / MRT4 (Skyra)', 'link': cl + 'B047.ics', 'eventColor': 'orange'},
        {'id': 'mrt5', 'title': 'MRT-TK / Tagesklinik', 'link': cl + 'MRT-TK.ics', 'eventColor': 'orange'},
        ]},

    {'id': 'eeg', 'title': 'EEG Labs',  'children': [
        {'id': 'eeg1', 'title': 'A004', 'link': cl + 'A004.ics', 'eventColor':'green'},
        {'id': 'eeg2', 'title': 'A134', 'link': cl + 'A134.ics', 'eventColor':'green'},
        {'id': 'eeg3', 'title': 'A138', 'link': cl + 'A138.ics', 'eventColor':'green'},
        {'id': 'eeg4', 'title': 'A228', 'link': cl + 'A228.ics', 'eventColor':'green'},
        {'id': 'eeg5', 'title': 'A229', 'link': cl + 'A229.ics', 'eventColor':'green'},
        {'id': 'eeg6', 'title': 'A230', 'link': cl + 'A230.ics', 'eventColor':'green'},
        {'id': 'eeg7', 'title': 'A231', 'link': cl + 'A231.ics', 'eventColor':'green'},
        {'id': 'eeg8', 'title': 'A232', 'link': cl + 'A232.ics', 'eventColor':'green'},
        {'id': 'eeg9', 'title': 'H0035', 'link': cl + 'H0035.ics', 'eventColor':'green'},
        {'id': 'eeg10', 'title': 'NeuroLab-UL', 'link': cl + 'NeuroLab-UL.ics', 'eventColor':'green'},
        ]
    },
    {'id': 'rt', 'title': 'RT Labs', 'children': [
        {'id': 'rt1', 'title': 'A127', 'link': cl + 'A127.ics', 'eventColor':'blue'},
        {'id': 'rt2', 'title': 'A128', 'link': cl + 'A128.ics', 'eventColor':'blue'},
        {'id': 'rt3', 'title': 'A129', 'link': cl + 'A129.ics', 'eventColor': 'blue'},
        {'id': 'rt4', 'title': 'A131', 'link': cl + 'A131.ics', 'eventColor': 'blue'},
        {'id': 'rt5', 'title': 'A225', 'link': cl + 'A225.ics', 'eventColor': 'blue'},
        {'id': 'rt6', 'title': 'A226', 'link': cl + 'A226.ics', 'eventColor': 'blue'},
        {'id': 'rt7', 'title': 'A227', 'link': cl + 'A227.ics', 'eventColor': 'blue'},
        {'id': 'rt8', 'title': 'A325', 'link': cl + 'A325.ics', 'eventColor': 'blue'},
        {'id': 'rt9', 'title': 'A326', 'link': cl + 'A326.ics', 'eventColor': 'blue'},
        {'id': 'rt10', 'title': 'A327', 'link': cl + 'A327.ics', 'eventColor': 'blue'},
        {'id': 'rt11', 'title': 'A334', 'link': cl + 'A334.ics', 'eventColor': 'blue'},
        {'id': 'rt12', 'title': 'A336', 'link': cl + 'A336.ics', 'eventColor': 'blue'},
        {'id': 'rt13', 'title': 'B022', 'link': cl + 'B022.ics', 'eventColor': 'blue'},

        ]},
    {'id': 'sl', 'title': 'Special Labs', 'children': [
        {'id': 'sl1', 'title': 'C105 (Mockup)', 'link': cl + 'C105.ics', 'eventColor': 'red'},
        {'id': 'sl2', 'title': 'A331 (VR Lab)', 'link': cl + 'A331.ics', 'eventColor': 'red'},
        {'id': 'sl3', 'title': 'C14 (VR lab)', 'link': cl + 'C14.ics', 'eventColor': 'red'},
        {'id': 'sl4', 'title': 'H1060 (MRT-PET NUK)', 'link': cl + 'H1060.ics', 'eventColor': 'red'},
        {'id': 'sl5', 'title': 'S408 (MEG Bennewitz)', 'link': cl + 'S408.ics', 'eventColor': 'red'},
        {'id': 'sl6', 'title': 'A332 (BabylabII)', 'link': cl + 'A332.ics', 'eventColor': 'red'},

    ]},
]

SCHEDULES_PATH = '/afs/cbs.mpg.de/tmp/apps/elkeserv/'