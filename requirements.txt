Click==7.0
Flask==1.0.2
icalendar==4.0.3
itsdangerous==1.1.0
Jinja2==2.10
MarkupSafe==1.1.0
npm==0.1.1
optional-django==0.1.0
python-dateutil==2.7.5
pytz==2018.9
six==1.12.0
Werkzeug==0.14.1
